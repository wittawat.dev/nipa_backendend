const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

//import Router
const ticketRouter = require('./routes/ticket')
app.use(cors());
app.use(express.json());
app.use('/ticket', ticketRouter);


app.get('/', (req,res) => {
    res.send('Hello World');
})

//connect
// const dburi = process.env.DB_CONNECTTION;
  const dburl  ="mongodb+srv://tonbee159:015665521@cluster0.z3mim4n.mongodb.net/test?retryWrites=true&w=majority";
  console.log("Connecting to DB...");
  mongoose.connect(dburl).catch(e => { 
    console.error("Error Connecting to DB"+e);
    console.log("Good Bye !");
    process.exit(1);
  });

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log('server is running on port '+PORT));