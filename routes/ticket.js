const express = require('express');
const router = express.Router();
const ticket = require('../models/ticket')

router.get('/', async (req,res) => {
  try {
      var result = await ticket.find().exec();
      res.send(result);
  }
  catch (error) {
      res.status(400).send({error});
  }
});
router.get('/:id', async (req,res) => {
  try {
      var result = await ticket.findOne({_id:req.params.id}).exec();
      res.send(result);
  }
  catch (error) {
      res.status(400).send({error});
  }
});
router.post('/', async (req,res) => {
  try {
      const newTicket = new ticket(req.body);
      const result = await newTicket.save();
      res.send(result);
  }
  catch (error) {
      res.status(400).send({error});
  }
});

router.put("/:id",  async (req, res) => {
  try {
      var e = await ticket.findOne({_id:req.params.id}).exec();
      e.set(req.body);
      var result = await e.save();
      res.send(result);
  } catch (error) { 
      res.status(400).send({error});
  }
});
module.exports = router;