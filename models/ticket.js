const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ticketSchema = new Schema({
        title: { type: String, required: false },
        description: { type: String, required: false},
        contact_information: { type: String, required: false },
        status: { type: String, required: false},
        active: { type: Boolean, required: false},
        create_date : { type: Date, default : Date.now },
        update_date : { type: Date, default : Date.now },
        deleted: { type: String, required: false},
})
ticketSchema.pre('save', function (next) {
        this.update_date = new Date();
        next();
});
module.exports = mongoose.model('ticket',ticketSchema)